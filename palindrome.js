//Idea
// 1. Input diubah jadi string supaya dapat index
// 2. ada var naik dan turun 
// 3. bandingkan secara looping (jika palindrome nilai 1 jika tidak 0)
// 4. batas i adalah length/2
// 4. push ke array sementara
// 6. cek array sementara.includes ada "0" return false jika tidak return true

// example case:
// ada 6 angkanya (length)
// 012345
// batas i berarti 2 => i < 3 atau length/2  

function palinD(input) {
    var numString = input.toString();
    var points = [];
    for (var i = 0; i < (numString.length/2); i++){
        var inc = i;
        var dec = numString.length - i - 1;
        if (numString[inc] == numString[dec]){
            points.push("1");
        }else{
            points.push("0");
        }
    }
    if(points.includes("0")){
        return false;
    } 
    else {
        return true;
    }
}

var number = 123221; //example false
var number2 = 984121489; //example true odd
var number3 = 9841221489; //example true even
var word = "SUMSARENOTSETASATESTONERASMUS"; //true
var word2 = "SUMSARENOTSETASATESTONERASMUS2"; //false
console.log(palinD(number));
console.log(palinD(number2));
console.log(palinD(number3));
console.log(palinD(word));
console.log(palinD(word2));



