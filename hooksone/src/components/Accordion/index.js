import React, { useState, useCallback } from "react";
import { FaChevronDown, FaChevronRight } from "react-icons/fa";

const Accordion = (props) => {
  const [open, setOpen] = useState(false);
  const openCloseAccordion = useCallback(() => {
    setOpen((old) => !old);
  }, []);
  return (
    // Accordion Component
    <div style={{ width: 700, padding: 15 }}>
      {/* Header Accordion, you can click, if you click header will expnad content and then if you click again content will close */}
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          border: "1px solid #fff",
          padding: 15,
        }}
        onClick={openCloseAccordion}
      >
        <div>{props.header}</div>
        <div>{open === false ? <FaChevronRight /> : <FaChevronDown />}</div>
      </div>

      {/* Contetnt Accordion */}
      {open === true ? (
        <div style={{ border: "1px solid gray", padding: 20 }}>
          {props.children}
        </div>
      ) : null}
    </div>
  );
};

export default Accordion;
