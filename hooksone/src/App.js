import React, { useCallback, useState } from "react";
import { FaBeer } from "react-icons/fa";
import "./App.css";
import { Accordion } from "./components";

function App() {
  const [auth, setAuth] = useState({ name: "Apnan Juanda", login: true });
  const authLoginLogout = useCallback(() => {
    if(auth.login === true){
      setAuth((previousValue) => {
        return {
          ...previousValue, name: '', login: !previousValue.login
        }
      })
    }
    else {
      setAuth((previousValue) => {
        return {
          ...previousValue, name: "Apnan Juanda", login: !previousValue.login
        }
      })
    }
  }, [auth.login]) //isi yang ada dalam [] itu adalah sesuatu yang selalu beruhah dan akan menjadi acuan untuk proses selanjutnya 
  return (
    <div className="App">
      <header className="App-header">
        {/* Login Information */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            padding: "15px 25px"
          }}
          onClick={authLoginLogout}
        >
          <div>{auth.login === true ? auth.name : 'Header'}</div>
          <div>{auth.login === true ? 'Log Out' : 'Log In'}</div>
        </div>

        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Accordion header="Lorem Ipsum">
            <p>
              Lorem, ipsum dolor sit amet consectetur adipisicing elit. Labore,
              quia, omnis esse consequatur molestiae ex neque eos, magnam
              inventore suscipit aspernatur nesciunt! Dolor neque, soluta
              pariatur quaerat reiciendis velit explicabo? Lorem ipsum dolor sit
              amet
            </p>
          </Accordion>
          <Accordion header="Jangan Lupa Minum">
            {" "}
            <FaBeer />{" "}
          </Accordion>
        </div>
      </header>
    </div>
  );
}

export default App;
