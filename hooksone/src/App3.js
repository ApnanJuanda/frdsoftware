import React, { useState, useCallback } from "react";
import "./App.css";
import { Accordion } from "./components";

function App() {
  const [auth, setAuth] = useState({ name: "Apnan Juanda", login: true });
  const authLoginLogout = useCallback(() => {
    if (auth.login === true) {
      setAuth((prev) => {
        return {
          ...prev,
          name: "",
          login: false,
        };
      });
    } else {
      setAuth((prev) => {
        return {
          ...prev,
          name: "Apnan Juanda",
          login: true,
        };
      });
    }
  }, [auth.login]);

  return (
    <div className="App">
      <header className="App-header">
        {/* Login Information */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            padding: "15px 25px",
          }}
        >
          <div>{auth.login === true ? auth.name : "Header"}</div>
          <div onClick={authLoginLogout}>
            {auth.login === true ? "Log Out" : "Log In"}
          </div>
        </div>
        <Accordion />
      </header>
    </div>
  );
}

export default App;
