//useReducer BASIC
// import React, { useReducer } from "react";
// import "./App.css";

// const initialState = { counter: 1, name: "apnan" };

// //state adalah callback
// //action adalah function/ rules
// const reducer = (state, action) => {
//   switch (action.type) {
//     case "INCREMENT":
//       return { ...state, counter: state.counter + 1 }; //spread operator ...
//     case "DECREMENT":
//       return { ...state, counter: state.counter - 1 }; //spread operator ...
//     case "CHANGE_NAME":
//       return { ...state, name: action.name }; //spread operator ...
//     default:
//       return state;
//   }
// };

// const App = () => {
//   const [state, dispatch] = useReducer(reducer, initialState);
//   return (
//     <div className="App">
//       <header className="App-header">
//         <div>{JSON.stringify(state)}</div>
//         <div style={{ display: "flex", flexDirection: "row", margin: 20 }}>
//           <input
//             value={state.name}
//             onChange={(event) =>
//               dispatch({ type: "CHANGE_NAME", name: event.target.value })
//             }
//           />
//           {/* <button>CHANGE NAME</button> */}
//         </div>
//         <div style={{ display: "flex", flexDirection: "row", margin: 20 }}>
//           <button
//             onClick={() => dispatch({ type: "DECREMENT" })}
//             style={{ height: 70, width: 40 }}
//           >
//             dec
//           </button>
//           <div style={{ margin: 20 }}>{state.counter}</div>
//           <button
//             onClick={() => dispatch({ type: "INCREMENT" })}
//             style={{ height: 70, width: 40 }}
//           >
//             inc
//           </button>
//         </div>
//       </header>
//     </div>
//   );
// };

// export default App;

import React, { useReducer } from "react";
//useReducer Advance
import "./App.css";

const initialState = {
  products: [
    { name: "indomie", qty: 5 },
    { name: "kopi", qty: 3 },
  ],
  checkout: [],
};

//state adalah callback
//action adalah function/ rules
const reducer = (state, action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      return {
        ...state,
        checkout:
          state.checkout.findIndex((x) => x.name === action.stuff) === -1
            ? [...state.checkout, { qty: 1, name: action.stuff }]
            : state.checkout.map((x) => {
                if (x.name === action.stuff) {
                  return { ...x, qty: x.qty + 1 };
                } else {
                  return x;
                }
              }),
        products: state.products.map((x) => {
          if (x.name === action.stuff) {
            return { ...x, qty: x.qty - 1 };
          } else {
            return x;
          }
        }),
      };
    case "REMOVE_FROM_CART":
      return {
        ...state,
        checkout:
          state.checkout.findIndex((x) => x.name === action.stuff) === -1
            ? [state.checkout]
            : state.checkout.map((x) => {
                if (x.name === action.stuff) {
                  return { ...x, qty: x.qty - 1 };
                } else {
                  return x;
                }
              }),
        products: state.products.map((x) => {
          if (x.name === action.stuff) {
            return { ...x, qty: x.qty + 1 };
          } else {
            return x;
          }
        }),
      };
    default:
      return state;
  }
};

const App = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <div className="App">
      <header className="App-header">
        <div>{JSON.stringify(state)}</div>
        <h3>Product</h3>
        <div>
          {state.products.map((product, index) => {
            return (
              <div key={index} style={{ width: 300, margin: 10 }}>
                {product.name}
                {state.checkout.findIndex((x) => x.name === product.name) === -1
                  ? [null]
                  : state.checkout.map((x) => {
                      if (x.name === product.name && x.qty > 0) {
                        return (
                          <button
                            style={{ width: 30, height: 30 }}
                            onClick={() =>
                              dispatch({
                                type: "REMOVE_FROM_CART",
                                stuff: product.name,
                              })
                            }
                          >
                            -
                          </button>
                        );
                      } else {
                        return null;
                      }
                    })}
                {product.qty}{" "}
                {product.qty > 0 ? (
                  <button
                    style={{ width: 30, height: 30 }}
                    onClick={() =>
                      dispatch({ type: "ADD_TO_CART", stuff: product.name })
                    }
                  >
                    +
                  </button>
                ) : null}
              </div>
            );
          })}
        </div>

        <h3>Checkout</h3>
        <div style={{ display: "flex", flexDirection: "row", margin: 20 }}>
          {state.checkout.map((stuff, index) => {
            if (stuff.qty > 0) {
              return (
                <div key={index} style={{ width: 300, margin: 10 }}>
                  {stuff.name} - {stuff.qty}
                </div>
              );
            } else {
              return null;
            }
          })}
        </div>
      </header>
    </div>
  );
};

export default App;
