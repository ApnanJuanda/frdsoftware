import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import CartContextProvider from "./context/CartContext";
import { CartPage, ProductPage } from "./pages";

const App = () => {
  return (
    <CartContextProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ProductPage />} />
          <Route path="/cart" element={<CartPage />} />
        </Routes>
      </BrowserRouter>
    </CartContextProvider>
  );
};

export default App;
