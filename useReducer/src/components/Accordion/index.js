import React, { useState, useCallback } from "react";
import { FaChevronRight, FaChevronDown } from "react-icons/fa";

const Accordion = (props) => {
  const [open, setOpen] = useState(false);
  const openCloseAccordion = useCallback(() => {
    setOpen((old) => !old);
  }, []);
  return (
    //Accordion
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      {/* Header Accordion */}
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-between",
          width: 700,
          border: "1px solid #fff",
          padding: 15,
        }}
        onClick={openCloseAccordion}
      >
        <div>{props.header}</div>
        <div>{open === false ? <FaChevronRight /> : <FaChevronDown />}</div>
      </div>

      {/* Content Accordion */}
      {open === true ? (
        <div
          style={{
            width: 700,
            border: "1px solid gray",
            padding: 15,
            alignItems: 'center'
          }}
        >
          <p style={{alignItems: 'center'}}>{props.children}</p>
        </div>
      ) : null}
    </div>
  );
};

export default Accordion;
