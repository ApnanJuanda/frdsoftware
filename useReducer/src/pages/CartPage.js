import React, { useMemo } from "react";
import { Link } from "react-router-dom";
import "../App.css";
import {
  useCartDispatchContext,
  useCartStateContext,
} from "../context/CartContext";

const CartPage = () => {
  const state = useCartStateContext();
  const dispatch = useCartDispatchContext();

  const totalQty = useMemo(() => {
    return {
      totalProduct: state.products.reduce((previousValue, currentValue) => {
        return previousValue + currentValue.qty;
      }, 0),
      totalCart: state.checkout.reduce((previousValue, currentValue) => {
        return previousValue + currentValue.qty;
      }, 0),
    };
  }, [state.products, state.checkout]);

  return (
    <div className="App">
      <header className="App-header">
        {console.log(state)}
        {/* Menu Page */}
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            margin: "10px 20px",
          }}
        >
          <h3>Cart ({totalQty.totalProduct})</h3>
          <Link style={{ color: "white" }} to="/">
            <h3>Back to Home</h3>
          </Link>
        </div>

        {/* Card Checkout */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
          }}
        >
          {state.checkout.map((product, index) => {
            return (
              <div key={index} style={{ width: 300, margin: 10 }}>
                <div>
                  <img
                    style={{ width: 200, height: 200 }}
                    alt=""
                    src={product.link}
                  />
                </div>

                <div>{product.name}</div>

                <div>
                  <button
                    style={{ width: 30, height: 30 }}
                    onClick={() =>
                      dispatch({
                        type: "REMOVE_FROM_CART",
                        stuff: product.name,
                      })
                    }
                  >
                    -
                  </button>
                  {product.qty}{" "}
                  <button
                    style={{ width: 30, height: 30 }}
                    onClick={() =>
                      dispatch({
                        type: "ADD_TO_CART",
                        stuff: product.name,
                        link: product.link,
                      })
                    }
                    disabled={
                      product.qty === product.defaultValue ? true : false
                    }
                  >
                    +
                  </button>
                </div>
              </div>
            );
          })}
        </div>

        {/* <h3>Checkout</h3>
        <div style={{ display: "flex", flexDirection: "row", margin: 20 }}>
          {state.checkout.map((stuff, index) => {
            if (stuff.qty > 0) {
              return (
                <div key={index} style={{ width: 300, margin: 10 }}>
                  {stuff.name} - {stuff.qty}
                </div>
              );
            } else {
              return null;
            }
          })}
        </div> */}
      </header>
    </div>
  );
};

export default CartPage;
