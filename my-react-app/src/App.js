import logo from "./logo.svg";
import "./App.css";
import { Body, Header, Footer } from "./components";

const App = () => {
  return (
    <div>
      {/* header */}
      <Header />

      {/* body */}
      <Body />

      {/* footer */}
      <Footer /> 
    </div>
  );
};

export default App;
