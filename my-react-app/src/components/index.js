import Body from "./Body";
import Header from "./Header";
import Footer from "./Footer";
import LinkHeader from "./LinkHeader";

export {Body, Header, Footer, LinkHeader};