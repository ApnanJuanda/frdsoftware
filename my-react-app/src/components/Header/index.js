import React from "react";
import { LinkHeader } from "..";


function Header() {
  return (
    <div className="header">
      <div>IGRACIAS</div>

      <div className="header">
        {/* <div>LOGIN</div> */}
        <LinkHeader src="/sso" className = "header">SSO</LinkHeader>
        <div style={{ margin: 30 }}></div>
        <LinkHeader src="/login">LOGIN</LinkHeader>
        <div style={{ margin: 30 }}></div>
        {/* <div>SIGNUP</div> */}
        <LinkHeader src="/signup">SIGNUP</LinkHeader>
      </div>
    </div>
  );
}

export default Header;
