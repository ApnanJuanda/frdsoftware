/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useCallback, useEffect, useMemo } from "react";
import axios from "axios";
import "./App.css";
import { Accordion } from "./components";

function App() {
  const [result, setResult] = useState([]);
  const [error, setError] = useState(false);
  const [search, setSearch] = useState("");

  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        setResult(res.data);
      })
      .catch((err) => {
        setError(true);
      })
  }, [])

  const [auth, setAuth] = useState({ name: "Apnan Juanda", login: true });
  const authLoginLogout = useCallback(() => {
    if (auth.login === true) {
      setAuth((prev) => {
        return {
          ...prev,
          name: "",
          login: false,
        };
      });
    } else {
      setAuth((prev) => {
        return {
          ...prev,
          name: "Apnan Juanda",
          login: true,
        };
      });
    }
  });

  const handleSearch = useCallback((e) => {
    setSearch(e.target.value);
  }, []);

  const searchList = useMemo(() => {
    return result.filter((data) => {
      return data.name.toLowerCase().includes(search.toLowerCase()); //ini ngecek datanya true ada nggak di result kalau ada baru di filter

    });
  }, [result, search]);

  return (
    <div className="App">
      <header className="App-header">
        {/* Login Information */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            padding: "15px 25px",
          }}
        >
          <div>{auth.login === true ? auth.name : "Username"}</div>
          <div onClick={authLoginLogout}>
            {auth.login === true ? "Log Out" : "Log In"}
          </div>
        </div>
        {/* Search Bar */}
        <div>
          <input
            value={search}
            onChange={handleSearch}
            style={{
              width: 500,
              height: 25,
              fontSize: 18,
              margin: 10,
              padding: 10,
            }}
          />
        </div>
        {/* Accordion */}
        {error === true ? (
          <div>Server Error</div>
        ) : result.length === 0 ? (
          <div>Loading</div>
        ) : (
          searchList.map((data, index) => {
            return (
              <Accordion header={data.name} key={index}>
                <p className="App-p">{data.phone}</p>
                <p className="App-p">{data.website}</p>
                <p className="App-p">{data.username}</p>
              </Accordion>
            );
          })
        )}
      </header>
    </div>
  );
}

export default App;
