import React, { useState, useCallback } from "react";
import { FaChevronRight, FaChevronDown} from "react-icons/fa";

const Accordion = (itu) => {
  const [open, setOpen] = useState(false);
  const openCloseAccordion = useCallback(() => {
    setOpen((old) => !old);
  }, []);
  return (
    //Accordion
    <div
      style={{ display: "flex", flexDirection: "column", alignItems: "center" }}
    >
      {/* Header Accordion */}
      <div
        style={{
          width: 700,
          padding: 15,
        }}
      >
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            border: "1px solid #fff",
            padding: 15,
          }}
          onClick={openCloseAccordion}
        >
          <div>{itu.header}</div>
          <div>{open === false ? <FaChevronRight /> : <FaChevronDown />}</div>
        </div>

        {/* Content Accordion */}
        {open === true ? (
          <div style={{ border: "1px solid gray", padding: 15, textAlign: 'left'}}>
            {itu.children}
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Accordion;
