/* eslint-disable array-callback-return */
import React from "react";

const Table = ({headers, results}) => {
  return (
    <div>
      <table>
        <thead>
          <tr>
            {headers.map((header) => {
              <th>{header}</th>;
            })}
          </tr>
        </thead>

        {/* <tbody>
          {props.results.map((result) => (
            <tr>
              {props.headers.map((header) => (
                <td>{result[header]}</td>
              ))}
            </tr>
          ))}
        </tbody> */}
      </table>
    </div>
  );
};

export default Table;
