/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useCallback, useEffect, useMemo } from "react";
import axios from "axios";
import "./App.css";
import { Accordion, Table } from "./components";

function App() {
  const [headers, setHeaders] = useState([]);
  const [results, setResults] = useState([]);
  const [isChecked, setIsChecked] = useState(false);
  const [isChecked2, setIsChecked2] = useState(false);
  const [error, setError] = useState(false);
  const [search, setSearch] = useState("");

  const [auth, setAuth] = useState({ name: "Apnan Juanda", login: true });
  const authLoginLogout = useCallback(() => {
    if (auth.login === true) {
      setAuth((prev) => {
        return {
          ...prev,
          name: "",
          login: false,
        };
      });
    } else {
      setAuth((prev) => {
        return {
          ...prev,
          name: "Apnan Juanda",
          login: true,
        };
      });
    }
  });

  //Peroleh Data dari API
  useEffect(() => {
    axios
      .get("https://jsonplaceholder.typicode.com/todos")
      .then((res) => {
        setResults(res.data);
        setHeaders(Object.keys(results[0]));
      })
      .catch((err) => {
        setError(true);
        console.log(err);
      });
  });

  //handleSearch
  const handleSearch = useCallback((e) => {
    setSearch(e.target.value);
  }, []);

  //handleCheckButtonTrue
  const checkButtonTrue = useCallback((e) => {
    setIsChecked(e.target.checked);
    console.log({ isChecked });
  }, []);

  //HandleCheckButtonFalse
  const checkButtonFalse = useCallback((e) => {
    setIsChecked2(e.target.checked);
    console.log(`Button ke-2: ${isChecked2}`);
  }, []);

  const searchList = useMemo(
    (e) => {
      if (
        (isChecked === false && isChecked2 === false) ||
        (isChecked === true && isChecked2 === true)
      ) {
        return results.filter((value) => {
          return value.title.toLowerCase().includes(search.toLowerCase());
        });
      } else if (
        (isChecked === false && isChecked2 === true) ||
        (isChecked === true && isChecked2 === false)
      ) {
        let data = results.filter((data) => data.completed === isChecked);
        return data.filter((value) => {
          return value.title.toLowerCase().includes(search.toLowerCase());
        });
      }
    },
    [results, search, isChecked]
  );

  return (
    <div className="App">
      <header className="App-header">
        {/* Login Information */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            padding: "15px 25px",
          }}
        >
          <div>{auth.login === true ? auth.name : "Username"}</div>
          <div onClick={authLoginLogout}>
            {auth.login === true ? "Log Out" : "Log In"}
          </div>
        </div>

        {/* Header Search Bar */}
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
          }}
        >
          <div
            style={{
              justifyContent: "space-between",
              width: 1000,
              display: "flex",
              flexDirection: "row",
              alignItems: "center",
              padding: 15,
              marginBottom: 25,
            }}
          >
            {/* Checkbox Button */}
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: 200,
                padding: 10,
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <label>True </label>
                <input
                  type="checkbox"
                  checked={isChecked}
                  onChange={checkButtonTrue}
                  style={{
                    height: 20,
                    width: 20,
                    margin: 10,
                  }}
                />
              </div>

              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <label>False </label>
                <input
                  type="checkbox"
                  checked={isChecked2}
                  onChange={checkButtonFalse}
                  style={{
                    height: 20,
                    width: 20,
                    margin: 10,
                  }}
                />
              </div>
            </div>
            <div>
              <h2>Tugas Search List</h2>
            </div>

            <div>
              <input
                style={{ height: 30, width: 400, padding: 5, fontSize: 20 }}
                onChange={handleSearch}
                value={search}
              />
            </div>
          </div>
        </div>

        {/* Content Table */}
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            textAlign: "left",
          }}
        >
          <table>
            <thead>
              {headers.map((header, index) => {
                return (
                  <th style={{ textAlign: "center", border: '3px solid gray' }} key={index}>
                    {header}
                  </th>
                );
              })}
            </thead>
            <tbody>
              {searchList.map((result, index) => {
                return (
                  <tr key={index}>
                    {/* {headers.map((header) => (
                      <td>{result[header]}</td>
                    ))} */}
                    <td>{result.userId}</td>
                    <td>{result.id}</td>
                    <td>{result.title}</td>
                    <td>{result.completed === true ? "True" : "False"}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </header>
    </div>
  );
}

export default App;
