import React, { useState, createContext, useEffect } from "react";

export const ThemeContext = createContext();

const ThemeProvider = ({ children }) => {
  const [mode, setMode] = useState("dark");
  const SwitchMode = (e, mode2) => {
    if (mode2 === "dark") {
      setMode("light");
    } else {
      setMode("dark");
    }
    localStorage.setItem("mode", mode)
  };

  useEffect(() => {
    let localStorageMode = localStorage.getItem("mode")
    if(localStorageMode === "dark"){
      setMode("light")
    }else{
      setMode("dark")
    }
  }, [])

  return (
    <ThemeContext.Provider
      value={{
        mode,
        SwitchMode,
      }}
    >
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;
