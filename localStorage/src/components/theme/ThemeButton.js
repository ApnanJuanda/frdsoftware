import React, { useContext } from "react";
import { ThemeContext } from "../theme/ThemeProvider";
import { FaMoon, FaSun } from "react-icons/fa";

const ThemeButton = () => {
  const { mode, SwitchMode } = useContext(ThemeContext);

  return (
    <div>
      {mode === "dark" ? (
        <FaSun style={{width: 40, height: 40}} onClick={(e) => SwitchMode(e, mode)} />
      ) : (
        <FaMoon style={{width: 40, height: 40}} onClick={(e) => SwitchMode(e, mode)} />
      )}
    </div>
  );
};

export default ThemeButton;
