import React, { useMemo } from "react";
import { useCartStateContext } from "../../context/CartContext";
import ActionCartCard from "./ActionCartCard";

const RenderCart = () => {
  const state = useCartStateContext();
  return useMemo(() => {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
        }}
      >
        {state.checkout.map((product, index) => {
          return (
            <div key={index} style={{ width: 300, margin: 10 }}>
              <div>
                <img
                  style={{ width: 200, height: 200 }}
                  alt=""
                  src={product.link}
                />
              </div>

              <div>{product.name}</div>
              <ActionCartCard product={product} />
            </div>
          );
        })}
      </div>
    );
  }, [state.checkout]);
  //Card Checkout
};

export default RenderCart;
