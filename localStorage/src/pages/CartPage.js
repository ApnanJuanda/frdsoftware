import React, { useContext, useMemo } from "react";
import { Link } from "react-router-dom";
import "../App.css";
import RenderCart from "../components/cart/RenderCart";
import ThemeButton from "../components/theme/ThemeButton";
import { ThemeContext } from "../components/theme/ThemeProvider";
import { useCartStateContext } from "../context/CartContext";

const CartPage = () => {
  const state = useCartStateContext();
  const { mode } = useContext(ThemeContext);

  const totalQty = useMemo(() => {
    return {
      totalProduct: state.products.reduce((previousValue, currentValue) => {
        return previousValue + currentValue.qty;
      }, 0),
      totalCart: state.checkout.reduce((previousValue, currentValue) => {
        return previousValue + currentValue.qty;
      }, 0),
    };
  }, [state.products, state.checkout]);

  return useMemo(() => {
    return (
      <div className={mode === "dark" ? "App-dark" : "App"}>
        <header className="App-header">
          {console.log(state)}
          {/* Menu Page */}
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              margin: "10px 0px -30px 20px",
            }}
          >
            <ThemeButton />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "10px 20px",
            }}
          >
            <h3>Cart ({totalQty.totalProduct})</h3>
            <Link style={{ color: "white" }} to="/">
              <h3 style={{ color: mode === "dark" ? "white" : "#282c34" }}>
                Back to Home
              </h3>
            </Link>
          </div>

          <RenderCart />
        </header>
      </div>
    );
  }, [mode, state, totalQty.totalProduct]);
};

export default CartPage;
