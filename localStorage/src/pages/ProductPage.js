import React, { useContext, useMemo } from "react";
import { Link } from "react-router-dom";
import "../App.css";
import RenderProduct from "../components/product/RenderProduct";
import ThemeButton from "../components/theme/ThemeButton";
import { ThemeContext } from "../components/theme/ThemeProvider";
import { useCartStateContext } from "../context/CartContext";

const ProductPage = () => {
  const state = useCartStateContext();
  const { mode } = useContext(ThemeContext);

  const totalQty = useMemo(() => {
    return {
      totalProduct: state.products.reduce((previousValue, currentValue) => {
        return previousValue + currentValue.qty;
      }, 0),
      totalCart: state.checkout.reduce((previousValue, currentValue) => {
        return previousValue + currentValue.qty;
      }, 0),
    };
  }, [state.products, state.checkout]);

  return useMemo(() => {
    return (
      <div className={mode === "dark" ? "App-dark" : "App"}>
        <header className="App-header">
          {console.log(state)}
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              margin: "10px 0px -30px 20px",
              
            }}
          >
            <ThemeButton />
          </div>

          {/* Menu Page */}
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              margin: "10px 20px",
            }}
          >
            <h3>Product ({totalQty.totalProduct})</h3>
            <Link style={{ color: "white" }} to="/cart">
              <h3 style={{ color: mode === "dark" ? "white" : "#282c34" }}>
                Checkout ({totalQty.totalCart})
              </h3>
            </Link>
          </div>
          <RenderProduct />
        </header>
      </div>
    );
  }, [mode, state, totalQty.totalCart, totalQty.totalProduct]);
};

export default ProductPage;
