import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import  ThemeProvider from "./components/theme/ThemeProvider";
import CartContextProvider from "./context/CartContext";
import { CartPage, ProductPage } from "./pages";

const App = () => {
  return (
    <ThemeProvider>
      <CartContextProvider>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<ProductPage />} />
            <Route path="/cart" element={<CartPage />} />
          </Routes>
        </BrowserRouter>
      </CartContextProvider>
    </ThemeProvider>
  );
};

export default App;
